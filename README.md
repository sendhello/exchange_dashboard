# Exchange Dashboard
Сервис графического отображения курсов биржи

### Зависимости
* python 3.10+

### Установка зависимостей
Установка `poetry` (если отсутствует)
```commandline
pip install poetry
```
Добавление виртуального окружения и установка зависимостей
```commandline
poetry install --no-dev
```
То же самое с утилитами разработки
```commandline
poetry install
```

### Запуск сервиса
##### в виртуальном окружении:
```shell
python main.py
```
##### в docker-compose:
```shell
docker-compose -f deploy/docker/docker-compose.yml up
```
Дашбоард будет доступен по адресу http://127.0.0.1:8000

### Переменные окружения
* `SERVER_URL` - url подключения к api биржи
* `DEBUG` - Режим отладки
* `HOST` - хост, по-умолчанию 0.0.0.0
* `PORT` - порт, по-умолчанию 5000

### Запуск линтеров:
```shell
tools/hooks/pre-commit
```
