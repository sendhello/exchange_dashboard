import asyncio
import json
from typing import Any

import pandas as pd  # type: ignore
import plotly.express as px  # type: ignore
from dash import Dash, Input, Output, dcc, html  # type: ignore

from .config import SERVER_URL
from .request import request
from .styles import block_style

app = Dash(__name__)

app.layout = html.Div([
    html.H1('Coins analysis'),
    html.H3('Current coin: n/a', id='title'),
    html.Div(
        [
            html.P('Select coin:'),
            dcc.Dropdown(
                id='coin_selector',
                options=[],
                value=1,
                clearable=False,
            ),
        ],
        style=block_style,
    ),
    dcc.Graph(id='graph'),
    dcc.Store(id='coin_list'),
    dcc.Store(id='coin_id'),
    dcc.Interval(
        id='interval',
        interval=1 * 1000,
        n_intervals=0,
    ),
])


def get_coin_data(coin_id: str) -> dict:
    """Запрос данных по монете по ее ID.
    """
    resp = asyncio.run(request(f'{SERVER_URL}/coins/{coin_id}'))
    return resp


@app.callback(Output('coin_list', 'data'), Input('coin_selector', 'value'))
def save_title(_: str) -> str:
    """Сохранение списка монет в кеш.
    """
    resp = asyncio.run(request(f'{SERVER_URL}/coins'))
    return json.dumps([{'label': el['title'], 'value': el['id']} for el in resp])


@app.callback(Output('coin_selector', 'options'), Input('coin_list', 'data'))
def update_title(data: str) -> list:
    """Вывод отсортированного списка монет из кеша.
    """
    return sorted(json.loads(data), key=lambda x: x['label'])


@app.callback(Output('coin_id', 'data'), Input('coin_selector', 'value'))
def update_dataframe(coin_id: str) -> str:
    """Сохранение ID монеты в кеш при переключении селектора.
    """
    return coin_id


@app.callback(
    Output('title', 'children'),
    Output('graph', 'figure'),
    Input('coin_id', 'data'),
    Input('interval', 'n_intervals'),
)
def display_data(coin_id: str, _: int) -> tuple[str, Any]:
    """Получение ID монеты из кеша и обновление данных по монете.
    """
    resp = get_coin_data(coin_id)
    df = pd.DataFrame(resp['rates'])
    fig = px.line(df, x='timestamp', y='rate')
    return f"Current coin: {resp['title']}", fig
